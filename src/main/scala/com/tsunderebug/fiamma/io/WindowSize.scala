import scala.scalanative.native.UShort

package com.tsunderebug.fiamma {

  package io {

    case class WindowSize(rows: UShort, columns: UShort, width: UShort, height: UShort)

  }

}